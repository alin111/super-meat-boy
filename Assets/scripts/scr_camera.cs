﻿using UnityEngine;
using System.Collections;

public class scr_camera : MonoBehaviour { 

	private Transform tofind;

	// Use this for initialization
	void Start () {
		tofind = GameObject.Find ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (tofind.position.x, tofind.position.y, transform.position.z);
	}
}
