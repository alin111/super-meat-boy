﻿using UnityEngine;
using System.Collections;

public class scr_door : MonoBehaviour {

	private SpriteRenderer rendSpr;

	public Sprite sprOff;
	public Sprite sprOn;
	public Sprite sprOpen;

	private bool touch;
	private src_switch switchState;

	// Use this for initialization
	void Start () {
		rendSpr = gameObject.GetComponent<SpriteRenderer> ();
		switchState = GameObject.FindGameObjectWithTag ("Switch").GetComponent<src_switch> ();
		touch = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (switchState.getActive ()) {
			rendSpr.sprite = sprOn;
		} else {
			rendSpr.sprite = sprOff;
		}
	}


}
