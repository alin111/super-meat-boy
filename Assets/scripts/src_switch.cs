﻿using UnityEngine;
using System.Collections;

public class src_switch : MonoBehaviour {

	private SpriteRenderer rendSpr;

	public Sprite sprOff;
	public Sprite sprOn;

	private bool touch;

	// Use this for initialization
	void Start () {
		rendSpr = gameObject.GetComponent<SpriteRenderer> ();
		touch = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (!touch) {
			rendSpr.sprite = sprOff;
		} else {
			rendSpr.sprite = sprOn;
		}
	}

	public bool getActive(){
		return touch;
	}

	public void setActive(){
		touch = true;
	}
}
