﻿using UnityEngine;
using System.Collections;

public class scr_Player : MonoBehaviour {

	//Variables publicas
	public float  MoveSpeed;
	public float JumpSpeed;
	public float MaxJump;
	public Sprite spriteJump;

	//Variables privadas
	private float saveMoveSpeed;
	private bool noMoveDch;
	private bool noMoveIzq;

	private bool ground;
	private bool jump;
	private bool slice;
	private bool jumpFinish;
	private Rigidbody2D bodyVelocity;
	private int direction;
	private Transform bodyPosition;
	private float initJumpPosition;
	private float finalJumpPosition;
	private BoxCollider2D bodyCollision;
	private bool doubleJump;
	private float countJump;

	private bool contactDch;
	private bool contactIzq;
	private Animator anim;

	private float collisionOffsetY;
	private float collisionSizeY;

	private float checkpointX;
	private float checkpointY;

	private bool dead;
	private float timeRespawn;

	private float movingPlataform;

	// Use this for initialization
	void Start () {
		//Componente de Rigidbody Player
		bodyVelocity = gameObject.GetComponent<Rigidbody2D>();
		//Componente de Transform Player
		bodyPosition = gameObject.GetComponent<Transform>();
		//Componente del sprite Player
		bodyCollision = gameObject.GetComponent<BoxCollider2D>();
		//Componente animator
		anim = gameObject.GetComponent<Animator>();
		//Direccion Player
		direction = 0;
		jump = false;
		jumpFinish = false;
		ground = false;
		saveMoveSpeed = MoveSpeed;
		doubleJump = true;
		countJump = 0;
		dead = false;
		slice = false;

		checkpointX = bodyPosition.position.x;
		checkpointY = bodyPosition.position.y;

		collisionOffsetY = bodyCollision.offset.y;
		collisionSizeY = bodyCollision.size.y;

	}
	
	// Update is called once per frame
	void Update () {
		if (!dead) {
			setInput ();
			player_Movement ();
		} else {
			if ((Time.time - timeRespawn) > 5) {
				bodyPosition.position = new Vector2 (checkpointX, checkpointY);
				anim.SetBool ("Dead", false);
				anim.SetBool ("Run", true);
				dead = false;
			}
		}
	}

	void setInput(){
		if (Input.GetButtonDown ("Fire1") && ground && !slice) {
			anim.SetBool ("Slice", true);
			slice = true;
			if (bodyVelocity.velocity.x > 0) {
				bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x + 3, bodyVelocity.velocity.y);
			} else if (bodyVelocity.velocity.x < 0) {
				bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x - 3, bodyVelocity.velocity.y);
			}
			bodyCollision.offset = new Vector2 (bodyCollision.offset.x, -0.60f);
			bodyCollision.size = new Vector2 (bodyCollision.size.x, 0.8f);
		}

		//Movimiento Horizontal
		if (Input.GetAxis ("Horizontal") > 0) {
			direction = 1;
			if (!jump) {
				anim.SetBool ("Run", true);
			}
			bodyPosition.localScale = new Vector3 ( 1 , 1 , 1);
		} else if (Input.GetAxis ("Horizontal") < 0) {
			direction = -1;
			if (!jump) {
				anim.SetBool ("Run", true);
			}
			bodyPosition.localScale = new Vector3 ( -1 , 1 , 1); 
		} else{
			direction = 0;
			anim.SetBool ("Run", false);
			if (bodyVelocity.velocity.x == 0) {
				anim.SetBool ("Slice", false);
				slice = false;
			}
		}


		//Movimiento Vertical
		if (Input.GetButtonDown ("Vertical") && (ground || doubleJump)) {
			jump = true;
			anim.SetBool ("Jump", true);
			anim.SetBool ("Slice", false);
			slice = false;
			countJump += 1;
			if (countJump >= 2) {
				doubleJump = false;
				countJump = 0;
			}
			jumpFinish = false;
			//Posicion Debajo de los pies 
			initJumpPosition = bodyPosition.position.y - bodyCollision.size.y / 2;
			finalJumpPosition = initJumpPosition + MaxJump;
		}

		if (!slice) {
			bodyCollision.offset = new Vector2 (bodyCollision.offset.x, collisionOffsetY);
			bodyCollision.size = new Vector2 (bodyCollision.size.x, collisionSizeY);
		}
	}

	void player_Movement(){
		//Movimiento Horizontal 
		if (direction == 1 && !noMoveDch) {
			if (slice) {
				bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x, bodyVelocity.velocity.y);
			} else {
				bodyVelocity.velocity = new Vector2 (MoveSpeed * direction, bodyVelocity.velocity.y);
			}
		} else if (direction == -1 && !noMoveIzq) {
			if (slice) {
				bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x, bodyVelocity.velocity.y);
			} else {
				bodyVelocity.velocity = new Vector2 (MoveSpeed * direction, bodyVelocity.velocity.y);
			}
		} else if (direction == 0 && !slice) {
			bodyVelocity.velocity = new Vector2 (MoveSpeed * direction, bodyVelocity.velocity.y);
		} else if (slice) {
			bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x, bodyVelocity.velocity.y);
		}
		//Movimiento Vertical
		if (jump) {
			if (!jumpFinish && (finalJumpPosition - (bodyPosition.position.y - bodyCollision.size.y / 2)) > 0.75) {
				bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x, JumpSpeed - JumpSpeed * (((bodyPosition.position.y - bodyCollision.size.y / 2) - initJumpPosition) / MaxJump));
			} else {
				jumpFinish = true;
				bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x, bodyVelocity.velocity.y);
				if (bodyVelocity.velocity.y == 0) {
					jump = false;
					jumpFinish = false;
				}
			}
		} else if (bodyVelocity.velocity.y < 0) {
			anim.SetBool ("Run", false);
		}

		bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x + movingPlataform, bodyVelocity.velocity.y);
	}

	void OnCollisionStay2D(Collision2D other){
		if (other.gameObject.tag == "Ground" && other.gameObject.layer == 8) {
			movingPlataform = other.rigidbody.velocity.x;
		}
	}

	void OnCollisionEnter2D (Collision2D other) {
		//Contact GROUND
		if (other.gameObject.tag == "Ground") {
			foreach (ContactPoint2D contactHit in other.contacts) {
				Vector2 hitPoint = contactHit.point;
				if (hitPoint.y > bodyPosition.position.y) {
					jump = false;
					bodyVelocity.velocity = new Vector2 (bodyVelocity.velocity.x, 0);
				} else {
					ground = true;
					doubleJump = true;
					anim.SetBool ("Jump", false);
				}
			}
		}
		//Contact WALL
		if (other.gameObject.tag == "Wall") {
			//MoveSpeed = 0;
			doubleJump = true;
			foreach (ContactPoint2D contactHit in other.contacts) {
				Vector2 hitPoint = contactHit.point;
				if (hitPoint.x > bodyPosition.position.x) {
					noMoveDch = true;
					contactDch = true;
				} else {
					noMoveIzq = true;
					contactIzq = true;
				}
			}
		}

		if (other.gameObject.tag == "Dead") {
			dead = true;
			anim.SetBool ("Jump", false);
			anim.SetBool ("Run", false);
			anim.SetBool ("Dead", true);
			anim.SetBool ("Slice", false);
			timeRespawn = Time.time;
		}
	}

	void OnCollisionExit2D (Collision2D other) {
		//Exit contact GROUND
		if (other.gameObject.tag == "Ground") {
			ground = false;
			movingPlataform = 0;
		}
		//Exit contact WALL
		if (other.gameObject.tag == "Wall") {
			MoveSpeed = saveMoveSpeed;
			noMoveDch = false;
			noMoveIzq = false;
			if (contactDch) {
				contactDch = false;
				bodyVelocity.velocity = new Vector2 (-3, bodyVelocity.velocity.y); 
			}
			if (contactIzq) {
				contactIzq = false;
				bodyVelocity.velocity = new Vector2 (3, bodyVelocity.velocity.y); 
			}
		}
	}

	void OnTriggerStay2D (Collider2D other) {
		if (other.gameObject.tag == "Switch" && Input.GetButtonDown("Fire2")) {
			other.gameObject.GetComponent<src_switch> ().setActive ();
			checkpointX = other.gameObject.transform.position.x;
			checkpointY = other.gameObject.transform.position.y;
			Debug.Log ("ENTRO SWITCH");
		}
		if (other.gameObject.tag == "Dead") {
			dead = true;
			anim.SetBool ("Jump", false);
			anim.SetBool ("Run", false);
			anim.SetBool ("Slice", false);
			anim.SetBool ("Dead", true);
		}
	}
}