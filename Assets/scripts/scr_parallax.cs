﻿using UnityEngine;
using System.Collections;

public class scr_parallax : MonoBehaviour {

	public float vel;

	private Rigidbody2D playerVel;
	private float tiempoInicio;

	private float tiempoParado;

	private bool initTime;
	private bool inTime;

	private Renderer rend;

	private float guardadoOffset;

	// Use this for initialization
	void Start () {
		playerVel = GameObject.FindGameObjectWithTag ("Player").GetComponent<Rigidbody2D> ();
		initTime = true;
		rend = GetComponent<Renderer> ();
		inTime = true;
		guardadoOffset = 0;
	}

	// Update is called once per frame
	void Update () {

		if ((playerVel.velocity.x > 0 || playerVel.velocity.x < 0) && initTime) {
			initTime = false;
			inTime = true;
			tiempoInicio = Time.time;
			//Debug.Log ("Tiempo");
		}

		if (playerVel.velocity.x < 0) {
			rend.material.mainTextureOffset = new Vector2 ((guardadoOffset - (((Time.time - tiempoInicio) * vel))) % 1, 0);
			guardadoOffset = rend.material.mainTextureOffset.x;
			initTime = false;
		}

		if (playerVel.velocity.x > 0) {
			rend.material.mainTextureOffset = new Vector2 ((guardadoOffset + (((Time.time - tiempoInicio) * vel))) % 1, 0);
			guardadoOffset = rend.material.mainTextureOffset.x;
			initTime = false;
		}

		if (playerVel.velocity.x == 0) {
			initTime = true;
		}
	}


}
