﻿using UnityEngine;
using System.Collections;

public class src_movingPlataform : MonoBehaviour {

	public int speed;

	private int direction;
	private Rigidbody2D platformBody;

	// Use this for initialization
	void Start () {
		direction = -1;
		platformBody = gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		platformBody.velocity = new Vector2 (speed * direction, platformBody.velocity.y);
	}

	void OnTriggerEnter2D (Collider2D other){
		if (other.gameObject.tag == "ChangeDirection") {
			direction = direction * -1;
		}
	}
}
